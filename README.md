# Indicium Airflow Hands-on Tutorial
Este é o desafio de Airflow da Indicium feito pelo membro da lighthouse Pedro Simon.

## O Desafio
O desafio contém a execução de um pipeline de dados orquestrado via Airflow que de forma simples, coleta dados de duas tabelas do banco de dados da Northwind, alocado localmente em /data/Northwind_small.sqlite, cria um arquivo csv com uma destas tabelas, e posteriormente faz um join entre ambas, extraindo a quantidade de produtos vendidos com entrega para o Rio de Janeiro

## Tasks criadas

Ao total, o pipeline contém 3 tasks, 2 desenvolvidas pelo autor e 1 task já criada para o desafio que entrega os valores de forma padrão em um arquivo .txt.
Serão apresentadas abaixo o funcionamento de todas elas:

### Task 1 - create_csv_order
Esta tarefa extrai a tabela "Order" do banco de dados da Northwind e aloca seus dados em um csv chamado output_order.csv (dentro da pasta data)

### Task 2 - generate_count.doc_md
Esta tarefa realiza um "LEFT JOIN" entre a tabela sql OrderDetails e a tabela csv output_orders.csv. Após isso, calcula a quantidade de produtos vendidos com entrega para a cidade de Rio de Janeiro e extrai essa informação para o arquivo count.txt

### Task 3 - export_final_output

Esta tarefa recolhe o email de uma variável do airflow chamada "my_email" e o resultado das tarefas anteriores (contidas em count.txt) e as insere em um arquivo final_output.txt com as informações codificadas por ascii.

## Dependências das Tasks

As tasks criadas possuem uma dependência linear definida de seguinte maneira:

create_csv_order >> generate_count >> export_final_output

Isto é, iniciando na esquerda, a tarefa seguinte inicia-se apenas após a conclusão da anterior.