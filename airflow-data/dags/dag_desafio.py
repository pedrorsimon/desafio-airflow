from airflow.utils.edgemodifier import Label
from datetime import datetime, timedelta
from textwrap import dedent
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow import DAG
from airflow.models import Variable

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}


## Do not change the code below this line ---------------------!!#
def export_final_answer():
    import base64

    # Import count
    with open('count.txt') as f:
        count = f.readlines()[0]

    my_email = Variable.get("my_email")
    message = my_email+count
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    with open("final_output.txt", "w") as f:
        f.write(base64_message)
    return None
## Do not change the code above this line-----------------------##


def create_csv_order():
    import pandas as pd
    import sqlite3

    # connecting with Northwind db with Python
    con = sqlite3.connect('data/Northwind_small.sqlite')

    # getting order table inside a pandas dataframe
    order = pd.read_sql_query('SELECT * FROM "order"', con)

    # saving it to a csv file
    order.to_csv("./data/output_orders.csv", index=False)

    return None


def generate_count():
    import pandas as pd
    import sqlite3

    # connecting with Northwind db with Python
    con = sqlite3.connect('data/Northwind_small.sqlite')

    # getting both tables as pandas DataFrames
    OrderDetail = pd.read_sql_query('SELECT * FROM OrderDetail', con)
    Order = pd.read_csv('./data/output_orders.csv')

    # Left Join between OrderDetail and Order
    MergedTables = OrderDetail.merge(Order.rename(
        {'Id': 'OrderId'}, axis=1), on='OrderId', how='left')

    # Subseting df to conditions (ShipCity = 'Rio de Janeiro')
    subset_orders = MergedTables[MergedTables['ShipCity'] == 'Rio de Janeiro']

    # Sum of the quantities
    result = subset_orders['Quantity'].sum()

    # Generating a count.txt with result
    with open("count.txt", "w+") as f:
        f.write(str(result))

    return None


with DAG(
    'DesafioAirflow',
    default_args=default_args,
    description='Desafio de Airflow da Indicium',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:
    dag.doc_md = """
        Esse é o desafio de Airflow da Indicium.
    """

    export_final_output = PythonOperator(
        task_id='export_final_output',
        python_callable=export_final_answer,
        provide_context=True
    )
    export_final_output.doc_md = dedent(
        """\
    #### Task Documentation

    This task gets the email from the airflow variable "my_email" and the result of previous tasks inside the count.txt file 
    and fetches an enconded message with those informations inside final_output.txt.
    """
    )

    create_csv_order = PythonOperator(
        task_id='create_csv_order',
        python_callable=create_csv_order,
        provide_context=True
    )
    create_csv_order.doc_md = dedent(
        """\
    #### Task Documentation

    This task extracts the "Order" table from Northwind_small.sqlite DB and fetches it into a csv file named output_orders.csv (inside data folder)
    """
    )

    generate_count = PythonOperator(
        task_id='generate_count',
        python_callable=generate_count,
        provide_context=True
    )
    generate_count.doc_md = dedent(
        """\
    #### Task Documentation

    This task "left joins" information from the sql table named OrderDetails and the csv file named output_orders.csv.
    After that, it calculates the quantity of sold products with shipping to Rio de Janeiro and exctracts that info to the count.txt file.
    """
    )

    create_csv_order >> generate_count >> export_final_output
